from models import Customers, session

class database:
    def showUsers(self):
        try:
            result = session.query(Customers)
            for row in result:
                print(row.userid, row.namadepan, row.namabelakang, row.email)
        except Exception as e:
            print(e)
        pass
    def showUserById(self, **params):
        try:
            result = session.query(Customers).filter(Customers.userid==params['userid'])
            for row in result:
                print(row.userid, row.namadepan, row.namabelakang, row.email)
        except Exception as e:
            print(e)
        pass
    def insertUser(self,**params):
        session.add(Customers(**params))
        session.commit()
        pass
    def updateUserById(self,**params):
        result =  session.query(Customers).filter(Customers.userid==params['userid']).one()
        result.username = params['username']
        session.commit()
        pass
    def deleteUserById(self,**params):
        result =  session.query(Customers).filter(Customers.userid==params['userid']).one()
        session.delete(result)
        session.commit()
        pass

if __name__ == "__main__":
    db = database()
    
    db.showUsers()

    params = {"userid":2}
    db.showUserById(**params)

    params = {
        "userid":5,
        "username":"ta",
        "namadepan":"Taufik",
        "namabelakang":"Adi",
        "email":"123@321.com"
    }
    db.insertUser(**params)

    params = {
        "userid":5,
        "username":"taufikadi",
    }
    db.updateUserById(**params)

    params = {
        "userid":5,
    }
    db.deleteUserById(**params)
